package com.whoopitup.netrunner.cardmngr;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.whoopitup.netrunner.cardmngr.model.Card;
import com.whoopitup.netrunner.cardmngr.model.CardDatabase;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 */
public class CardDatabaseImporter {
    public void importDatabase(CardDatabase database, String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.readTree(json);
        for (JsonNode c : rootNode.get("cards")) {
            Card card = parseCard(c);
            database.addCard(card);
        }
    }

    private Card parseCard(JsonNode c) {
        Card card = new Card();
        card.name = c.get("name").textValue().trim();
        card.number = Integer.parseInt(c.get("num").textValue());
        card.set = c.get("set").textValue();

        card.set = card.set.replaceAll("&#39;", "'");

        return card;
    }
}
