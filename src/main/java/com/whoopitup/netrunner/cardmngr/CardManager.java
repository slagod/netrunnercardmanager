package com.whoopitup.netrunner.cardmngr;

import com.whoopitup.netrunner.cardmngr.model.Card;
import com.whoopitup.netrunner.cardmngr.model.CardDatabase;
import com.whoopitup.netrunner.cardmngr.model.CardList;
import com.whoopitup.netrunner.cardmngr.model.CardListItem;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.util.List;

/**
 */
public class CardManager {

    private CardDatabase database;
    private DeckImporter deckImporter;

    public CardManager() throws IOException {
        setupDatabase();

        deckImporter = new DeckImporter();
        deckImporter.database = database;
    }

    //~

    public String getDeckBuildingInstructions(String deck) throws IOException {
        CardList list = deckImporter.importDeck(deck);
        return new DeckFormatter().formatDeck(list);
    }

    public String switchDecks(String from, String to) throws IOException {
        CardList fromDeck = deckImporter.importDeck(from);
        CardList toDeck = deckImporter.importDeck(to);

        for (CardListItem toCard : toDeck.cards) {
            for (CardListItem c : fromDeck.cards) {
                if (c.card.name.equals(toCard.card.name)) {
                    toCard.quantity -= c.quantity;
                    c.quantity = 0;
                    break;
                }
            }
        }
        for (CardListItem fromCard : fromDeck.cards) {
            if (fromCard.quantity > 0) {
                toDeck.addCard(fromCard.card, -fromCard.quantity);
            }
        }

        return new DeckFormatter().formatDeck(toDeck);
    }

    public String combineDecks(List<String> decks) throws IOException {
        CardList result = new CardList();
        for (String input : decks) {
            CardList deck = deckImporter.importDeck(input);

            for (CardListItem cli : deck.cards) {
                boolean found = false;
                for (CardListItem c : result.cards) {
                    if (c.card.name.equals(cli.card.name)) {
                        c.quantity = Math.max(c.quantity, cli.quantity);
                        found = true;
                    }
                }
                if (!found)
                    result.addCard(cli.card, cli.quantity);
            }
        }

        return new DeckFormatter().formatDeck(result);
    }

    //~

    private void setupDatabase() throws IOException {
        database = new CardDatabase();

        CardDatabaseImporter databaseImporter = new CardDatabaseImporter();

        String corp = IOUtils.toString(Main.class.getClassLoader().getResourceAsStream("corpcards.json"));
        databaseImporter.importDatabase(database, corp);

        String runner = IOUtils.toString(Main.class.getClassLoader().getResourceAsStream("runnercards.json"));
        databaseImporter.importDatabase(database, runner);
    }

}
