package com.whoopitup.netrunner.cardmngr;

import com.whoopitup.netrunner.cardmngr.model.CardList;
import com.whoopitup.netrunner.cardmngr.model.CardListItem;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

/**
 */
public class DeckFormatter {

    private static final Map<String, Integer> CardSetOrder;

    static {
        CardSetOrder = new HashMap<>();

        CardSetOrder.put("Core", 1);

        CardSetOrder.put("What Lies Ahead", 2);
        CardSetOrder.put("Trace Amount", 3);
        CardSetOrder.put("Cyber Exodus", 4);
        CardSetOrder.put("A Study in Static", 5);
        CardSetOrder.put("Humanity's Shadow", 6);
        CardSetOrder.put("Future Proof", 7);

        CardSetOrder.put("Creation and Control", 8);

        CardSetOrder.put("Opening Moves", 9);
    }

    public String formatDeck(CardList deck) {
        List<CardListItem> cards = new ArrayList<>();
        cards.addAll(deck.cards);

        Collections.sort(cards, new Comparator<CardListItem>() {
            @Override
            public int compare(CardListItem o1, CardListItem o2) {
                int v1 = CardSetOrder.get(o1.card.set) * 1000 + o1.card.number;
                int v2 = CardSetOrder.get(o2.card.set) * 1000 + o2.card.number;
                return v1 - v2;
            }
        });

        String currentSet = null;

        StringWriter sw = new StringWriter();
        PrintWriter p = new PrintWriter(sw);
        for (CardListItem cli : cards) {
            if (cli.quantity == 0)
                continue;

            if (!cli.card.set.equals(currentSet)) {
                p.println();
                p.println(cli.card.set);
                currentSet = cli.card.set;
            }

            p.println(String.format("%dx %03d %s", cli.quantity, cli.card.number, cli.card.name));
        }
        return sw.toString();
    }
}
