package com.whoopitup.netrunner.cardmngr;

import com.whoopitup.netrunner.cardmngr.model.Card;
import com.whoopitup.netrunner.cardmngr.model.CardDatabase;
import com.whoopitup.netrunner.cardmngr.model.CardList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 */
public class DeckImporter {

    private static final Pattern PATTERN = Pattern.compile("^((\\d)x )?([\\w\\s\\d:,\"'-]+) \\(");

    public CardDatabase database;

    public CardList importDeck(String input) throws IOException {

        CardList deck = new CardList();

        BufferedReader r = new BufferedReader(new StringReader(input));
        String line = null;
        while ((line = r.readLine()) != null) {
            Matcher m = PATTERN.matcher(line);
            if (!m.find())
                continue;

            Card card = database.findCardByName(m.group(3).trim());
            if (card == null)
                continue; // todo: log

            int quantity = 1;

            if (!deck.cards.isEmpty()) {
                if (m.group(2) == null)
                    continue;
                quantity = Integer.parseInt(m.group(2));
            }

            deck.addCard(card, quantity);
        }

        return deck;
    }



}
