package com.whoopitup.netrunner.cardmngr;

import com.whoopitup.netrunner.cardmngr.model.CardDatabase;
import com.whoopitup.netrunner.cardmngr.model.CardList;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        CardManager app = new CardManager();

        String cmd = args[0];
        if (cmd.equals("build")) {
            String deck = IOUtils.toString(new FileInputStream(new File(args[1])));
            System.out.println(app.getDeckBuildingInstructions(deck));
        } else if (cmd.equals("switch")) {
            String from = IOUtils.toString(new FileInputStream(new File(args[1])));
            String to = IOUtils.toString(new FileInputStream(new File(args[2])));
            System.out.println(app.switchDecks(from, to));
        } else if (cmd.equals("combine")) {
            List<String> decks = new ArrayList<>();
            for (int i = 1; i < args.length; i++) {
                decks.add(IOUtils.toString(new FileInputStream(new File(args[i]))));
            }
            System.out.println(app.combineDecks(decks));
        }
    }
}
