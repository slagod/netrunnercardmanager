package com.whoopitup.netrunner.cardmngr.model;

import com.whoopitup.netrunner.cardmngr.model.Card;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 */
public class CardDatabase {

    public List<Card> cards = new ArrayList<Card>();

    private Map<String, Card> indexByName = new HashMap<>();

    //~

    public void addCard(Card card) {
        cards.add(card);

        indexByName.put(card.name, card);
    }

    public Card findCardByName(String name) {
        return indexByName.get(name);
    }
}
