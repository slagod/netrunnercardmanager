package com.whoopitup.netrunner.cardmngr.model;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class CardList {
    public List<CardListItem> cards = new ArrayList<CardListItem>();

    public void addCard(Card card, int quantity) {
        cards.add(new CardListItem(card, quantity));
    }
}
