package com.whoopitup.netrunner.cardmngr.model;

/**
 */
public class CardListItem {
    public Card card;
    public Integer quantity;

    public CardListItem(Card card, int quantity) {
        this.card = card;
        this.quantity = quantity;
    }
}
